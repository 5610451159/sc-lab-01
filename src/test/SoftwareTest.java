package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import model.Card;
import gui.SoftwareFrame;

public class SoftwareTest {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SoftwareTest();
	}

	public SoftwareTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setBounds(500, 100, 400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		/*
		 * Student may modify this code and write your result here
		 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
		 */
		Card m = new Card(0);
		double dp = 1000 ;
		double wd = 200 ;
		frame.setResult("Balance : " + m.getBalance(0) + "�");
		m.deposit(dp) ;
		frame.extendResult("Deposit : " + dp + "�");
		frame.extendResult("Balance : " + m.getBalance(0) + "�");
		m.withdraw(wd) ;
		frame.extendResult("Withdraw : " + wd + "�");
		frame.extendResult("Balance : " + m.getBalance(0) + "�");

	}

	ActionListener list;
	SoftwareFrame frame;
}
